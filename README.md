# render-json-2-table

a mini package that render JSON object into a table (you define how to render a table), this package does the nested-object rendering logic

## Warning
This is very alpha release, DO NOT USE IT. This readme is not ready, and the code is not ready.

## Getting Started
The `Dockerfile` and `docker-compose.yml` are designed for development

## How-to-use
renderCell and renderTable needs to be able to handle null

## Docker-based development
Dependencies:
 - Official node image
 - running node container as user=node

Start the development machine:

```
sudo docker-compose up
```

Enter the shell

```
sudo docker-compose exec node101 /bin/bash
```




## Checklists

 - [x] setup docker dev environment
 - [x] setup typescript + jest
 - [x] use generic types to replace any
 - [ ] prepare docker-based development using insider vscode
