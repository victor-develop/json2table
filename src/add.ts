
export default function add(
    x: number,
    y: number
): number {
    return Number(x) + Number(y)
}
