/**
 *  node -r ts-node/register src/playground/index.ts
 */

import { convertJson2HtmlTable2 } from '../htmlTable/ConverJson2HtmlTable'
import {exampleArray} from '../__fixtures__/jsons/example'
// eslint-disable-next-line @typescript-eslint/no-var-requires
const  prettifyHtml = require('prettify-html')

// eslint-disable-next-line no-console
const s = console.log

const html = `
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootswatch/4.4.1/sketchy/bootstrap.min.css" />
${convertJson2HtmlTable2(
    [
        ...exampleArray,
        null
    ]
)}
`

s(prettifyHtml(
    html
))
