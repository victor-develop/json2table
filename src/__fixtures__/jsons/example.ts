const example = {
    city: {
        name: 'Hong Kong',
        continent: 'Asia',
        people: {
            write: 'Traditional Chinese',
            speak: 'Cantonese'
        }
    },
    city2: {
        name: 'Singapore',
        continent: 'Asia',
        people: {
            write: 'Singalish',
            speak: 'Cantonese/Singalish/Others'
        }
    }
}

export const exampleArray = [
    example,
    {
        city: {
            name: 'Macau',
            continent: 'Asia',
            people: {
                write: 'Traditional Chinese',
                speak: 'Cantonese'
            }
        },
        city3: {
            name: 'Malaysia',
            continent: 'Asia',
            people: {
                write: 'Traditional Chinese',
                speak: 'Cantonese'
            }
        },
    }
]

export default example
