export enum BodyCellTypes {
    BASIC = 'basic',
    COMPOSITE = 'composite'
}

export const headerCellTypeValue = 'headerCell'

export type HeaderCellType = typeof headerCellTypeValue

export interface HeaderCell {
    type: HeaderCellType;
    headerName: string;
}

export const Symbols = {
    BodyCell: Symbol('BodyCell'),
}

export interface BodyCell<ResultTableT> {
    symbolType: typeof Symbols.BodyCell;
    type: BodyCellTypes;
    headerName: string;
    content: ResultTableT | string | number | null;
}

export type Cell<ResultTableT> = HeaderCell | BodyCell<ResultTableT>

export type Row<ResultTableT> = HeaderCell[] | BodyCell<ResultTableT>[]

export type BasicRow<ResultTableT> = BodyCell<ResultTableT>[]

export type HeaderRow = HeaderCell[]

export interface Table<ResultTableT> {
    tableName: string;
    headers: HeaderRow;
    rows: BasicRow<ResultTableT>[];
}

interface Options {
    tableName?: string;
    headers?: HeaderRow;
}


export type ConvertJson2Table<ResultTableT> = (json: object, options?: Options) => ResultTableT | null

export interface TableRenders<ResultTableT, ResultCell, ResultRow, ResultRows, ResultHeaderSection> {
    renderCell: (c: BodyCell<ResultTableT> | ResultTableT) => ResultCell;
    renderRow: (r: Row<ResultTableT>) => ResultRow;
    renderHeaderSection: (headerRow: HeaderRow) => ResultHeaderSection;
    renderBodySection: (rs: Row<ResultTableT>[]) => ResultRows;
    renderTable: (
        renderHeaderSection: (headerRow: HeaderRow) => ResultHeaderSection,
        renderBodySection: (rs: Row<ResultTableT>[]) => ResultRows
    ) => ResultTableT;
}

export type ConvertJson2TableFactory<ResultTableT = any, ResultCell = any, ResultRow = any, ResultRows = any, ResultHeaderSection = any> = (
    options: TableRenders<ResultTableT, ResultCell, ResultRow, ResultRows, ResultHeaderSection>
) => ConvertJson2Table<ResultTableT>
