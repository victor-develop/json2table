import { convertJson2TableFactory } from '../converJson2TableFactory'
import { BodyCell, Row, HeaderRow, Symbols } from '../contracts'


const renderCell = (c: BodyCell<string>) =>
    `<td>${c ? (c.symbolType === Symbols.BodyCell && c.content || c) : ''}</td>`
const renderRow = (cells: Row<string>) =>
    `<tr>${cells.join('')}</tr>`
const renderHeaderSection = (headCells: HeaderRow) => '<tr>' + headCells.map(h => `<th>${h.headerName}</th>`).join('') + '</tr>'
const renderBodySection = (rs: Row<string>[]) => `<tbody>${rs.join('')}</tbody>`
const renderTable = (headerSection: any, bodySection: any) => `<table class="table table-bordered"><thead>${headerSection}</thead>${bodySection}</table>`

export const convertJson2HtmlTable2 = convertJson2TableFactory(
    {
        renderCell,
        renderRow,
        renderHeaderSection,
        renderBodySection,
        renderTable,
    }
)
export default convertJson2HtmlTable2
