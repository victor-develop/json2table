import { convertJson2HtmlTable2 } from './../ConverJson2HtmlTable'
import prettifyHtml from 'prettify-html'
import example, {exampleArray} from '../../__fixtures__/jsons/example'

describe('convertJson2HtmlTable', () => {
    test('convert non-array json to HTML table', () => {
        const result = convertJson2HtmlTable2(example)
        expect(prettifyHtml(result)).toMatchSnapshot('html table output')
    })
    test('convert array json to HTML table', () => {
        const result = convertJson2HtmlTable2(exampleArray)
        expect(prettifyHtml(result)).toMatchSnapshot('html table output')
    })
    test('convert array json with null items to HTML table', () => {
        const result = convertJson2HtmlTable2([...exampleArray, null])
        expect(prettifyHtml(result)).toMatchSnapshot('html table output')
    })
})
