import { ConvertJson2Table, HeaderRow, headerCellTypeValue, BodyCell, BodyCellTypes, BasicRow, ConvertJson2TableFactory, Symbols } from './contracts/index'
import isArray from 'lodash/isArray'
import uniqBy from 'lodash/uniqBy'
import isUndefined from 'lodash/isUndefined'

function extractObjHeader(json: object): HeaderRow {
    if (json) {
        const headers: HeaderRow = Object.keys(json)
            .map(key => ({
                type: headerCellTypeValue,
                headerName: key
            }))
        return headers
    }
    return []
}

type ResultTable = any

export const convertJson2TableFactory: ConvertJson2TableFactory<ResultTable> = (
    { renderCell,
        renderRow,
        renderHeaderSection,
        renderBodySection,
        renderTable }
) => {
    const convertJson2Table: ConvertJson2Table<ResultTable> = (json, options = {}) => {
        const { headers: inheritedHeaders = [] } = options
        if (isArray(json)) {
            const initialTable: SemiTable = {
                headers: inheritedHeaders,
                renderedRowList: []
            }
            interface SemiTable {
                headers: HeaderRow;
                renderedRowList: any[];
            }
            const tb: SemiTable = json.reduce((mergedTable: SemiTable, item) => {
                // BUG: array of arrays will encounter trouble at this place
                const itemHeaders = extractObjHeader(item)
                const mergedHeaders = uniqBy(mergedTable.headers.concat(itemHeaders), x => x.headerName)
                // const keys = item ? [] : Object.keys(item)
                const row = renderRow(mergedHeaders
                    .map(h => {
                        const cellContent = item ? item[h.headerName] : null
                        if (cellContent) {
                            return renderCell(convertJson2Table(cellContent))
                        }
                        return renderCell(null)
                    }))
                const merge = {
                    headers: uniqBy(mergedHeaders.concat(itemHeaders), x => x.headerName),
                    renderedRowList: [...mergedTable.renderedRowList, row]
                }
                return merge
            }, initialTable)
            return renderTable(
                renderHeaderSection(tb.headers),
                renderBodySection(tb.renderedRowList)
            )
        }

        if (isUndefined(json) || json === null || typeof json !== 'object') {
            return null
        }

        const headers: HeaderRow = Object.keys(json)
            .map(key => ({
                type: headerCellTypeValue,
                headerName: key
            }))
        const row: BasicRow<ResultTable> = Object.entries(json).map(([key, value]: any[]) => {
            const headerName = key
            if (isArray(value)) {
                const arrayCell: BodyCell<ResultTable> = {
                    symbolType: Symbols.BodyCell,
                    headerName,
                    type: BodyCellTypes.COMPOSITE,
                    content: convertJson2Table(value, { tableName: headerName })
                }
                return renderCell(arrayCell)
            }
            if (value !== null && typeof value === 'object') {
                const objCell: BodyCell<ResultTable> = {
                    symbolType: Symbols.BodyCell,
                    headerName,
                    type: BodyCellTypes.COMPOSITE,
                    content: convertJson2Table(value, { tableName: headerName })
                }
                return renderCell(objCell)
            }
            const cell: BodyCell<ResultTable> = {
                symbolType: Symbols.BodyCell,
                headerName,
                type: BodyCellTypes.BASIC,
                content: value
            }
            return renderCell(cell)
        })
        return renderTable(
            renderHeaderSection(headers),
            renderBodySection([renderRow(row)])
        )
    }
    return convertJson2Table
}
